public class TrafficLight {
    private final static int[] DIRECTION = {1, 2, 3};
    private final static int RED_LAMP = 120;
    private final static int ORANGE_LAMP = 5;

    public static void main(String[] args){
        System.out.println("RED LAMP T2 IS ON");
        System.out.println("RED LAMP T3 IS ON");

        while (true){
            int counter = 0;
            for(int i = 0; i < DIRECTION.length; i++){
                counter = 0;
                String TLNow = "T" +  DIRECTION[i];
                System.out.println("RED LAMP " + TLNow + " IS OFF");
                try{
                    if(DIRECTION[i] == 1){
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i+2] + " KE T" + DIRECTION[i]  + " BOLEH LANGSUNG");
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i+1] + " KE T" + DIRECTION[i+2]  + " BOLEH LANGSUNG");
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i+1] + " KE T" + DIRECTION[i] + " HARAP MENUNGGU LAMPU HIJAU");
                    } else if(DIRECTION[i] == 3){
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i-1] + " KE T" + DIRECTION[i]  + " BOLEH LANGSUNG");
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i-2] + " KE T" + DIRECTION[i-1]  + " BOLEH LANGSUNG");
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i-2]  + " KE " + TLNow + " HARAP MENUNGGU LAMPU HIJAU");
                    } else {
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i-1] + " KE " + TLNow  + " BOLEH LANGSUNG");
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i+1] + " KE T" + DIRECTION[i-1]  + " BOLEH LANGSUNG");
                        System.out.println("KENDARAAN DARI T" + DIRECTION[i+1] + " KE " + TLNow + " HARAP MENUNGGU LAMPU HIJAU");
                    }
                } catch (ArrayIndexOutOfBoundsException e){
                    i = DIRECTION[0];
                }
                System.out.println("RED LAMP " + TLNow + " IS OFF");
                System.out.println("ORANGE LAMP " + TLNow + " IS ON");
                try {
                    Thread.sleep(ORANGE_LAMP * 1000);
                    System.out.println("ORANGE STATE");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("GREEN LAMP " + TLNow + " is ON");

                while (counter <= RED_LAMP/4 - ORANGE_LAMP){
                    counter++;
                    try {
                        Thread.sleep(1000);
                        System.out.println("GREEN " + counter);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(counter >= RED_LAMP/4 - ORANGE_LAMP){
                        System.out.println("GREEN " + TLNow + " IS OFF");
                        System.out.println("RED LAMP " + TLNow + " IS ON");
                        break;
                    }
                }
            }
        }
    }

}
